import CryptoJS from 'crypto-js';

const serverId = 10011218;
const secret = '4e7bb53ea62b472596db235e219860db';

const getDataType = data => Object.prototype.toString.call(data).match(/\[object (.*)\]/)[1];

const getSortObject = params => {
	if (getDataType(params) !== 'Object') {
		return;
	}

	let tempObj = {};
	Object.keys(params).forEach(key => {
		tempObj[key] = params[key];
	});
	return tempObj;
};

const getSign = params => {
	const sortParams = getSortObject(params);
	const str = `${JSON.stringify(sortParams)}${secret}`;
	const sign = CryptoJS.MD5(str).toString().toLowerCase();
	return sign;
};

export default {
	serverId,
	getSign
};
