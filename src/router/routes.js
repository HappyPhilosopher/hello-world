export default [
	{
		path: '/',
		redirect: 'home'
	},
	{
		path: '/home',
		name: 'home',
		component: () => import('@/views/test-home')
	},
	{
		path: '/about',
		name: 'about',
		component: () => import('@/views/test-about')
	}
];
