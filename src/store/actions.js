import axios from 'axios';

function getAdviserInfo(bid) {
	const protocol = window.location.protocol;
	const apiUrl = `${protocol}//test-data-platform.qh5800.com/api/2/h5/qq/dispatch.json?bid=${bid}`;

	return new Promise(resolve => {
		setTimeout(async () => {
			const { data } = await axios.get(apiUrl);
			resolve(data);
		}, 1000);
	});
}

export default {
	async getAdviserInfo({ commit }) {
		const adviserInfo = await getAdviserInfo('3510630');
		commit('GET_ADVISER_INFO', adviserInfo);
	}
};
