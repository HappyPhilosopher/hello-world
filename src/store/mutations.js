export default {
	GET_ADVISER_INFO(state, adviserInfo) {
		state.adviserInfo = Object.assign({}, state.adviserInfo, adviserInfo);
	}
};
