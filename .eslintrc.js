module.exports = {
	root: true,
	env: {
		node: true
	},
	extends: ['plugin:vue/essential', 'eslint:recommended', 'plugin:prettier/recommended'],
	parserOptions: {
		parser: '@babel/eslint-parser'
	},
	rules: {
		'prettier/prettier': [
			2,
			{
				singleQuote: true,
				printWidth: 120,
				tabWidth: 2,
				semi: true,
				bracketSpacing: true,
				trailingComma: 'none',
				arrowParens: 'avoid',
				useTabs: true,
				quoteProps: 'consistent',
				bracketSameLine: false,
				endOfLine: 'auto',
				htmlWhitespaceSensitivity: 'ignore',
				proseWrap: 'never'
			}
		]
	}
};
